package jap_c4_s1_exceptionhandling_pc2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OnlineComputerQuiz {
	
	String[] schoolNames = new String[3];
	String[] scores = new String[3];
	Scanner scan = new Scanner(System.in);
	
	void takingInput(){
		System.out.println("Enter all the schools:");
		for(int i=0; i<schoolNames.length; i++){
			try {
				schoolNames[i] = scan.nextLine();
			}
			catch(InputMismatchException e) {
				System.out.println(e);
			}
		}
		
		System.out.println("Enter the scores of all the schools:");
		for(int i=0; i<schoolNames.length; i++){
			try {
				scores[i] = scan.nextLine();
			}
			catch(InputMismatchException e) {
				System.out.println(e);
			}
		}
	}
	
	public String highestScoredSchool() {
		int ind = 0, maxScore=-1;
		for(int i=0; i<schoolNames.length; i++){
			try {
				int n = Integer.parseInt(scores[i]);
				if(n>maxScore) {
					maxScore = n;
					ind = i;
				}
			}
			catch(NumberFormatException e) {
				return e+ "";
			}
		}
		
		try {
			return schoolNames[ind];
		}
		catch(ArrayIndexOutOfBoundsException e) {
			return e+"";
		}
	}
	
	public String calculateAvgScore() {
		try {
			double sum=0.0;
			for(int i=0; i<scores.length; i++) {
				 sum += Integer.parseInt(scores[i]);
			}
			double avg =  sum/(double)scores.length;
			return String.valueOf(avg);	
		}
		catch(NumberFormatException e){
			return e + "";
		}
	}
	
	
	public static void main(String[] args) {
		OnlineComputerQuiz obj = new OnlineComputerQuiz();
		obj.takingInput();
		System.out.println("School with highest score is: " + obj.highestScoredSchool());
		System.out.println("Average score is: " + obj.calculateAvgScore());
	}
		
}
